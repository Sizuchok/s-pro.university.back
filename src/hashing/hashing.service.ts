import { Injectable } from '@nestjs/common';

@Injectable()
export abstract class HashingService {
  // abstract HashingService in case we want to switch the hashing algorithm in future
  abstract hash(data: string | Buffer): Promise<string>;
  abstract compare(data: string | Buffer, encrypted: string): Promise<boolean>;
}
