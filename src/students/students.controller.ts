import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  Query,
  HttpStatus,
  HttpCode,
  UseInterceptors,
  UploadedFile,
  ParseIntPipe,
} from '@nestjs/common';
import { StudentsService } from './students.service';
import { CreateStudentDto } from './dto/requests/create-student.dto';
import { UpdateStudentDto } from './dto/requests/update-student.dto';
import { StudentsPaginationQueryDto } from './dto/students-pagination-query.dto';
import {
  ApiBadRequestResponse,
  ApiBearerAuth,
  ApiBody,
  ApiConsumes,
  ApiCreatedResponse,
  ApiInternalServerErrorResponse,
  ApiNoContentResponse,
  ApiNotFoundResponse,
  ApiOkResponse,
  ApiOperation,
  ApiTags,
} from '@nestjs/swagger';
import { CreateStudentResponseDto } from './dto/responses/create-student-response.dto';
import { GetStudentResponseDto } from './dto/responses/get-student-response.dto';
import { AssignStudentToGroupDto } from './dto/requests/assign-student-to-group.dto';
import { UpdateStudentResponseDto } from './dto/responses/update-student-response.dto';
import { FileInterceptor } from '@nestjs/platform-express';
import { InternalServerErrorResponse } from '../application/dto/internal-server-error-response.dto';
import { uploadMiddleware } from '../application/middleware/upload.middleware';
import { AddImageForStudentRequestDto } from './dto/requests/add-image-for-student-request.dto';
import { GetAllStudentsWithCoursesAndCount } from './dto/responses/get-all-students-with-courses-and-count.dto';
import { Public } from '../auth/decorators/public-route.decorator';

@ApiBearerAuth()
@ApiInternalServerErrorResponse({
  description: 'Internal server error',
  type: InternalServerErrorResponse,
})
@ApiTags('students')
@Controller('students')
export class StudentsController {
  constructor(private readonly studentsService: StudentsService) {}

  @ApiOperation({ summary: 'Create a student' })
  @ApiBadRequestResponse({
    description:
      'Invalid name, surname, age, email or groupId. | Group id from request body was not found.',
  })
  @ApiCreatedResponse({
    type: CreateStudentResponseDto,
    description: 'A freshly created student with nulled imagePath and groupId fields.',
  })
  @Post()
  createStudent(@Body() createStudentDto: CreateStudentDto) {
    return this.studentsService.createStudent(createStudentDto);
  }

  @ApiOperation({ summary: 'Find all students (with query params)' })
  @ApiBadRequestResponse({
    description:
      'Invalid pagination query params, either: query, sortBy, order, limit or offset params',
  })
  @ApiOkResponse({
    type: GetAllStudentsWithCoursesAndCount,
    isArray: true,
    description: 'An array of student objects.',
  })
  @Get()
  findAllStudents(
    @Query() studentsPaginationQuery: StudentsPaginationQueryDto,
  ): Promise<GetAllStudentsWithCoursesAndCount> {
    return this.studentsService.findAllStudentsWithCourses(studentsPaginationQuery);
  }

  @ApiOperation({ summary: 'Find a student by id' })
  @ApiNotFoundResponse({ description: 'Student with { id } not found.' })
  @ApiBadRequestResponse({
    description: 'Invalid id param.',
  })
  @ApiOkResponse({
    type: GetStudentResponseDto,
    description: 'A student object.',
  })
  @Get(':id')
  findStudentById(@Param('id', ParseIntPipe) id: number) {
    return this.studentsService.findStudentById(id);
  }

  @ApiOperation({ summary: 'Update a student by id' })
  @ApiBadRequestResponse({
    description:
      'Invalid id, name, surname, email, age or groupId. | Student with { id } not found. | Group with { id } not found.',
  })
  @ApiOkResponse({
    type: GetStudentResponseDto,
    description: 'Updated student object',
  })
  @Patch(':id')
  updateStudentById(
    @Param('id', ParseIntPipe) id: number,
    @Body() updateStudentDto: UpdateStudentDto,
  ): Promise<GetStudentResponseDto> {
    return this.studentsService.updateStudentById(id, updateStudentDto);
  }

  @ApiOperation({ summary: 'Assign student to group' })
  @ApiBadRequestResponse({
    description:
      'Invalid student id param | Invalid courseId | Student with { id } not found. | Group with { id } not found.',
  })
  @ApiOkResponse({
    type: GetStudentResponseDto,
    description: 'Updated student object',
  })
  @Patch(':id/group')
  assignStudentToGroup(
    @Param('id', ParseIntPipe) id: number,
    @Body() assignStudentToGroupDto: AssignStudentToGroupDto,
  ): Promise<GetStudentResponseDto> {
    return this.studentsService.assignStudentToGroup(id, assignStudentToGroupDto);
  }

  @ApiOperation({ summary: 'Upload an image for a student' })
  @ApiBadRequestResponse({
    description: 'Invalid id param | Error uploading file.',
  })
  @ApiConsumes('multipart/form-data')
  @ApiBody({
    description: 'An image to be uploaded',
    type: AddImageForStudentRequestDto,
  })
  @ApiOkResponse({
    description: 'Student object with image local path',
    type: GetStudentResponseDto,
  })
  @Patch(':id/image')
  @UseInterceptors(FileInterceptor('file', uploadMiddleware))
  uploadFile(
    @Param('id', ParseIntPipe) id: number,
    @UploadedFile() file: Express.Multer.File,
  ): Promise<UpdateStudentResponseDto> {
    return this.studentsService.addImage(id, file.path);
  }

  @Public()
  @ApiOperation({ summary: 'Remove a student by id' })
  @ApiBadRequestResponse({
    description: 'Invalid id param. | Student with { id } not found.',
  })
  @ApiNoContentResponse({
    description: 'Student successfully removed.',
  })
  @HttpCode(HttpStatus.NO_CONTENT)
  @Delete(':id')
  removeStudentById(@Param('id') id: number) {
    return this.studentsService.removeStudentById(id);
  }
}
