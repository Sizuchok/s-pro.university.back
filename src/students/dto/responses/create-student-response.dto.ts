import { IsEmpty } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';
import { StudentEntityDto } from '../student-entity.dto';
import { CourseEntityDto } from '../../../courses/dto/course-entity.dto';

export class CreateStudentResponseDto extends StudentEntityDto {
  @ApiProperty({
    example: null,
  })
  @IsEmpty()
  readonly groupId: number;

  @ApiProperty({
    example: null,
  })
  @IsEmpty()
  readonly imagePath: string;

  readonly courses: CourseEntityDto[];
}
