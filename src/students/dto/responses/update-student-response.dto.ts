import { StudentEntityDto } from '../student-entity.dto';

export class UpdateStudentResponseDto extends StudentEntityDto {}
