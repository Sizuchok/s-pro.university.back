import { ApiProperty } from '@nestjs/swagger';
import { StudentEntityDto } from '../student-entity.dto';
import { CourseEntityDto } from '../../../courses/dto/course-entity.dto';

export class GetStudentResponseDto extends StudentEntityDto {
  @ApiProperty({
    nullable: true,
    example: 'SP-12',
  })
  readonly groupName: string;

  readonly courses: CourseEntityDto[];
}
