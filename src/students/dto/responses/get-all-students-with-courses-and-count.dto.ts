import { ApiProperty } from '@nestjs/swagger';
import { GetStudentResponseDto } from './get-student-response.dto';

export class GetAllStudentsWithCoursesAndCount {
  @ApiProperty()
  readonly students: GetStudentResponseDto[];

  @ApiProperty({
    example: 1,
  })
  readonly count: number;
}
