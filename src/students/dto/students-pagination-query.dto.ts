import { ApiPropertyOptional } from '@nestjs/swagger';
import { IsEnum, IsOptional, IsString } from 'class-validator';
import { BasePaginationQuery } from '../../application/dto/base-pagination-query.dto';
import { BaseSortBy } from '../../application/dto/enums/base-query.enums';

export const StudentsSortBy = {
  ...BaseSortBy,
  groupName: 'groupName',
  name: 'name',
} as const;

export class StudentsPaginationQueryDto extends BasePaginationQuery {
  @ApiPropertyOptional({
    description: 'A student`s name.',
    example: 'Michael',
  })
  @IsString()
  @IsOptional()
  readonly name: string;

  @ApiPropertyOptional({
    description: 'A field by which students list should be sorted',
    example: 'createdAt',
  })
  @IsOptional()
  @IsEnum(StudentsSortBy, { message: 'Invalid sortBy parameter' })
  readonly sortBy: string;
}
