import {
  ApiPropertyOptional,
  IntersectionType,
  OmitType,
  PartialType,
  PickType,
} from '@nestjs/swagger';
import { StudentEntityDto } from '../student-entity.dto';
import { IsArray, IsOptional } from 'class-validator';

export class CreateStudentDto extends IntersectionType(
  OmitType(StudentEntityDto, ['id', 'createdAt', 'updatedAt', 'imagePath'] as const),
  PartialType(PickType(StudentEntityDto, ['groupId'] as const)),
) {
  @ApiPropertyOptional({
    example: [1, 2, 3],
  })
  @IsArray()
  @IsOptional()
  courseIds?: number[];
}
