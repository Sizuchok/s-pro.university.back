import { PickType } from '@nestjs/swagger';
import { StudentEntityDto } from '../student-entity.dto';

export class AssignStudentToGroupDto extends PickType(StudentEntityDto, ['groupId'] as const) {}
