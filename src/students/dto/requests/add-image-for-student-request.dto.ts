import { ApiProperty } from '@nestjs/swagger';

export class AddImageForStudentRequestDto {
  @ApiProperty({ type: 'string', format: 'binary' })
  file: any;
}
