import { ApiPropertyOptional, OmitType, PartialType } from '@nestjs/swagger';
import { StudentEntityDto } from '../student-entity.dto';
import { IsArray, IsOptional } from 'class-validator';

export class UpdateStudentDto extends PartialType(
  OmitType(StudentEntityDto, ['id', 'createdAt', 'updatedAt', 'imagePath'] as const),
) {
  @ApiPropertyOptional({
    example: [1, 2, 3],
  })
  @IsArray()
  @IsOptional()
  courseIds?: number[];
}
