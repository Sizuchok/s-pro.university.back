import { ApiProperty } from '@nestjs/swagger';
import {
  IsEmail,
  IsInt,
  IsNumber,
  IsPositive,
  IsString,
  Max,
  MaxLength,
  Min,
  MinLength,
} from 'class-validator';
import { CoreEntityDto } from '../../application/dto/core-entity.dto';

export class StudentEntityDto extends CoreEntityDto {
  @ApiProperty({
    example: 'John',
  })
  @MaxLength(16)
  @MinLength(2)
  @IsString()
  readonly name: string;

  @ApiProperty({
    example: 'Doe',
  })
  @MaxLength(16)
  @MinLength(2)
  @IsString()
  readonly surname: string;

  @ApiProperty({
    format: 'email',
    example: 'john.doe@gmail.com',
  })
  @IsEmail()
  @IsString()
  readonly email: string;

  @ApiProperty({
    example: 17,
  })
  @Max(90)
  @Min(14)
  @IsInt()
  @IsNumber()
  readonly age: number;

  @ApiProperty({
    nullable: true,
    example: 1,
  })
  @IsInt()
  @IsPositive()
  @IsNumber()
  readonly groupId: number;

  @ApiProperty({
    nullable: true,
    example: 'students/64f331a92d2d880148e727cd.jpg',
  })
  @IsString()
  readonly imagePath: string;
}
