import {
  Injectable,
  InternalServerErrorException,
  NotFoundException,
  BadRequestException,
} from '@nestjs/common';
import { CreateStudentDto } from './dto/requests/create-student.dto';
import { UpdateStudentDto } from './dto/requests/update-student.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { Student } from './entities/student.entity';
import { FindManyOptions, ILike, In, Repository } from 'typeorm';
import { StudentsPaginationQueryDto, StudentsSortBy } from './dto/students-pagination-query.dto';
import { AssignStudentToGroupDto } from './dto/requests/assign-student-to-group.dto';
import { UpdateStudentResponseDto } from './dto/responses/update-student-response.dto';
import * as path from 'path';
import * as fs from 'fs/promises';
import * as fsSync from 'fs';
import ObjectID from 'bson-objectid';
import { Group } from '../groups/entities/group.entity';
import { Course } from '../courses/entities/course.entity';
import { CreateStudentResponseDto } from './dto/responses/create-student-response.dto';
import { GetStudentResponseDto } from './dto/responses/get-student-response.dto';
import { GetAllStudentsWithCoursesAndCount } from './dto/responses/get-all-students-with-courses-and-count.dto';

@Injectable()
export class StudentsService {
  constructor(
    @InjectRepository(Student)
    private readonly studentsRepository: Repository<Student>,
    @InjectRepository(Group)
    private readonly groupsRepository: Repository<Group>,
    @InjectRepository(Course)
    private readonly coursesRepository: Repository<Course>,
  ) {}

  private findStudentSelectQueryBuilder = async () => {
    const queryBuilder = this.studentsRepository
      .createQueryBuilder('student')
      .select([
        'student.id as id',
        'student.createdAt as "createdAt"',
        'student.updatedAt as "updatedAt"',
        'student.name as name',
        'student.surname as surname',
        'student.email as email',
        'student.age as age',
        'student.image_path as "imagePath"',
        'student.group_id as "groupId"',
      ])
      .leftJoin('student.group', 'group')
      .addSelect('group.name as "groupName"');

    return queryBuilder;
  };

  async createStudent({
    courseIds,
    ...studentData
  }: CreateStudentDto): Promise<CreateStudentResponseDto> {
    const groupId = studentData.groupId;

    if (groupId) {
      const group = await this.groupsRepository.findOneBy({ id: groupId });
      if (!group) throw new BadRequestException(`Group with { id: ${groupId} } not found.`);
    }

    const student = this.studentsRepository.create(studentData);

    if (courseIds?.length) {
      const courses = await this.coursesRepository.findBy({ id: In(courseIds) });
      student.courses = courses;
    }

    return this.studentsRepository.save(student);
  }

  async findAllStudents(
    studentsPaginationQuery: StudentsPaginationQueryDto,
  ): Promise<GetStudentResponseDto[]> {
    const queryBuilder = await this.findStudentSelectQueryBuilder();

    const { name, order, sortBy, query, limit, offset } = studentsPaginationQuery;

    if (name) queryBuilder.where('student.name = :name', { name });

    //the pagination dto has enum using which we validate the query params
    //so here we can recieve only a specific values both for the order and the sortBy props
    if (sortBy === StudentsSortBy.groupName && order)
      queryBuilder.orderBy(`group.name`, order === 'asc' ? 'ASC' : 'DESC');
    else if (order && sortBy)
      queryBuilder.orderBy(`student.${sortBy}`, order === 'asc' ? 'ASC' : 'DESC');

    if (query)
      queryBuilder
        .where('student.name ILIKE :query', { query: `%${query}%` })
        .orWhere('student.surname ILIKE :query', { query: `%${query}%` });

    queryBuilder.offset(offset).limit(limit);

    return queryBuilder.getRawMany();
  }

  async findAllStudentsWithCourses({
    name,
    limit,
    offset,
    order,
    query,
    sortBy,
  }: StudentsPaginationQueryDto): Promise<GetAllStudentsWithCoursesAndCount> {
    const findOptions: FindManyOptions<Student> = {
      skip: offset,
      take: limit,
      relations: ['courses', 'group'],
    };

    if (query)
      findOptions.where = [{ name: ILike(`%${query}%`) }, { surname: ILike(`%${query}%`) }];

    if (sortBy) findOptions.order = { [sortBy]: order };

    if (name) findOptions.where = { name };

    const students = await this.studentsRepository.find(findOptions);
    const formattedStudents = students.map(({ group, ...student }) => ({
      ...student,
      groupName: group ? group.name : null,
    }));

    return {
      students: formattedStudents,
      count: await this.studentsRepository.count(findOptions),
    };
  }

  async findStudentById(id: number): Promise<GetStudentResponseDto> {
    const student = await this.studentsRepository.findOne({
      where: { id },
      relations: ['courses', 'group'],
    });

    if (!student) throw new NotFoundException(`Student with { id: ${id} } not found`);

    const { group, ...data } = student;

    return {
      ...data,
      groupName: group ? group.name : null,
    };
  }

  async updateStudentById(
    id: number,
    { courseIds, ...updateStudentData }: UpdateStudentDto,
  ): Promise<GetStudentResponseDto> {
    const student = await this.studentsRepository.findOne({
      where: { id },
      relations: ['courses'],
    });

    if (!student) throw new BadRequestException(`Student with { id: ${id} } not found for update.`);

    if (courseIds?.length) {
      const courses = await this.coursesRepository.findBy({ id: In(courseIds) });
      student.courses = courses;
    } else {
      student.courses = [];
    }

    const groupId = updateStudentData.groupId;

    if (groupId) {
      const group = await this.groupsRepository.findOneBy({ id: groupId });
      if (!group) throw new BadRequestException(`Group with { id: ${groupId} } not found.`);
    }

    Object.assign(student, updateStudentData);

    const updatedStudent = await this.studentsRepository.save(student);

    if (!updatedStudent)
      throw new InternalServerErrorException('Error retrieving the updated student after update.');

    return this.findStudentById(updatedStudent.id);
  }

  async addImage(id: number, filePath: string): Promise<UpdateStudentResponseDto> {
    if (!filePath) throw new BadRequestException('File is not provided.');

    try {
      //if user is not found in db we want to delete image file from temp folder
      const student = this.studentsRepository.findOneBy({ id });

      if (!student) throw new BadRequestException(`Student with { id: ${id} } not found`);

      const imageId = ObjectID().toHexString();
      const imageExt = path.extname(filePath);

      if (!imageExt) throw new BadRequestException('Provided file has no extention or name.');

      const imageName = imageId + imageExt;

      const studentsFolderName = 'students';
      const studentsFolderPath = path.join(__dirname, '../', 'public', studentsFolderName);

      if (!fsSync.existsSync(studentsFolderPath)) fsSync.mkdirSync(studentsFolderPath);

      const studentImagePath = `${studentsFolderName}/${imageName}`;
      const newImagePath = path.join(studentsFolderPath, imageName);

      await fs.rename(filePath, newImagePath);

      const result = await this.studentsRepository.update(id, { imagePath: studentImagePath });

      if (!result.affected) throw new InternalServerErrorException('Internal server error.');

      const updatedStudent = await this.studentsRepository.findOneBy({ id });

      if (!updatedStudent)
        throw new InternalServerErrorException(
          'Error retrieving the updated student after update.',
        );

      return updatedStudent;
    } catch (error) {
      await fs.unlink(filePath);
      throw error;
    }
  }
  assignStudentToGroup(id: number, { groupId }: AssignStudentToGroupDto) {
    return this.updateStudentById(id, { groupId });
  }

  async removeStudentById(id: number): Promise<void> {
    const student = await this.studentsRepository.findOneBy({
      id,
    });

    if (!student)
      throw new BadRequestException(`Student with { id: ${id} } not found for deletion.`);

    const result = await this.studentsRepository.delete(id);

    if (!result.affected) throw new InternalServerErrorException('Internal server error.');
  }
}
