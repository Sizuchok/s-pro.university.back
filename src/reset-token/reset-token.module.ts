import { Module } from '@nestjs/common';
import { ResetTokenService } from './reset-token.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ResetToken } from './entities/reset-token.entity';
import { HashingModule } from '../hashing/hashing.module';

@Module({
  imports: [TypeOrmModule.forFeature([ResetToken]), HashingModule],
  providers: [ResetTokenService],
  exports: [ResetTokenService],
})
export class ResetTokenModule {}
