import { CoreEntity } from '../../application/entities/core.entity';
import { Lector } from '../../lectors/entities/lector.entity';
import { Column, Entity, JoinColumn, OneToOne } from 'typeorm';

@Entity('reset_tokens')
export class ResetToken extends CoreEntity {
  @OneToOne(() => Lector, {
    onDelete: 'CASCADE',
  })
  @JoinColumn({
    name: 'lector_id',
  })
  lector: Lector;

  @Column({ name: 'lector_id' })
  lectorId: number;

  @Column({
    type: 'varchar',
  })
  token: string;
}
