import { Injectable, Logger } from '@nestjs/common';
import * as crypto from 'crypto';
import { InjectRepository } from '@nestjs/typeorm';
import { ResetToken } from './entities/reset-token.entity';
import { Repository } from 'typeorm';
import { HashingService } from '../hashing/hashing.service';
import { GenerateResetTokenResponseDto } from './dto/generate-reset-token-response.dto';
import { ResetTokenEntityDto } from './dto/reset-token-entity.dto';

@Injectable()
export class ResetTokenService {
  private logger: Logger;
  constructor(
    @InjectRepository(ResetToken) private resetTokensRepository: Repository<ResetToken>,
    private hashingService: HashingService,
  ) {
    this.logger = new Logger(ResetTokenService.name);
  }

  public async generateResetToken(lectorId: number): Promise<GenerateResetTokenResponseDto> {
    const token = crypto.randomBytes(32).toString('hex');
    const hashedToken = await this.hashingService.hash(token);

    const existingToken = await this.resetTokensRepository.findOneBy({ lectorId });

    await this.resetTokensRepository.save({ ...existingToken, lectorId, token: hashedToken });

    return { lectorId, token };
  }

  public async getResetToken(lectorId: number): Promise<ResetTokenEntityDto> {
    return this.resetTokensRepository.findOneBy({ lectorId });
  }

  public async removeResetTokenById(id: number): Promise<void> {
    this.resetTokensRepository.delete(id);
  }
}
