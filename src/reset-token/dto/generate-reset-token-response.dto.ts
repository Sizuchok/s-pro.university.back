import { PickType } from '@nestjs/swagger';
import { ResetTokenEntityDto } from './reset-token-entity.dto';

export class GenerateResetTokenResponseDto extends PickType(ResetTokenEntityDto, [
  'lectorId',
  'token',
] as const) {}
