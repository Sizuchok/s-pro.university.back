import { ApiProperty } from '@nestjs/swagger';
import { IsInt, IsPositive, IsNumber, IsNotEmpty, IsString } from 'class-validator';
import { CoreEntityDto } from '../../application/dto/core-entity.dto';

export class ResetTokenEntityDto extends CoreEntityDto {
  @ApiProperty({ example: 27 })
  @IsInt()
  @IsPositive()
  @IsNumber()
  readonly lectorId: number;

  @ApiProperty({
    example: '116ceb69796ec022235631a61a8c0474796746fa7ada621777d2b0a543dfe88a',
  })
  @IsNotEmpty()
  @IsString()
  readonly token: string;
}
