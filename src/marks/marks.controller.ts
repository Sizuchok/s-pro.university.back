import {
  Controller,
  Get,
  Post,
  Body,
  Query,
  HttpStatus,
  Delete,
  Param,
  Patch,
  ParseIntPipe,
  HttpCode,
} from '@nestjs/common';
import { MarksService } from './marks.service';
import { CreateMarkDto } from './dto/requests/create-mark.dto';
import { MarksPaginationQueryDto } from './dto/marks-pagination-query.dto.ts';
import {
  ApiBadRequestResponse,
  ApiBearerAuth,
  ApiCreatedResponse,
  ApiExtraModels,
  ApiInternalServerErrorResponse,
  ApiNoContentResponse,
  ApiNotFoundResponse,
  ApiOkResponse,
  ApiOperation,
  ApiTags,
  getSchemaPath,
} from '@nestjs/swagger';
import { CreateMarkResponseDto } from './dto/responses/create-mark-response.dto';
import { GetMarkResposeDto } from './dto/responses/get-mark-response.dto';
import { GetMarkForCourseResponseDto } from './dto/responses/get-mark-for-course-response.dto';
import { GetMarkForStudentResponseDto } from './dto/responses/get-mark-for-student-response.dto';
import { InternalServerErrorResponse } from '../application/dto/internal-server-error-response.dto';
import { UpdateMarkDto } from './dto/requests/update-mark.dto';

@ApiBearerAuth()
@ApiExtraModels(GetMarkResposeDto, GetMarkForCourseResponseDto, GetMarkForStudentResponseDto)
@ApiInternalServerErrorResponse({
  description: 'Internal server error',
  type: InternalServerErrorResponse,
})
@ApiTags('marks')
@Controller('marks')
export class MarksController {
  constructor(private readonly marksService: MarksService) {}

  @ApiOperation({ summary: 'Create a mark' })
  @ApiBadRequestResponse({
    description: 'Invalid mark, studentId, courseId or lectorId.',
  })
  @ApiCreatedResponse({
    type: CreateMarkResponseDto,
    description: 'A freshly created mark object.',
  })
  @Post()
  createMark(@Body() createMarkDto: CreateMarkDto): Promise<CreateMarkResponseDto> {
    return this.marksService.createMark(createMarkDto);
  }

  @ApiOperation({ summary: 'Find all marks (with query param)' })
  @ApiBadRequestResponse({
    description: 'Invalid id param, studentId or courseId.',
  })
  @ApiOkResponse({
    description:
      'Depending on a query param returns marks all marks, marks for student or for a course',
    content: {
      'application/json': {
        schema: {
          oneOf: [
            { type: 'array', $ref: getSchemaPath(GetMarkResposeDto) },
            { type: 'array', $ref: getSchemaPath(GetMarkForCourseResponseDto) },
            { type: 'array', $ref: getSchemaPath(GetMarkForStudentResponseDto) },
          ],
        },
        examples: {
          GetMarkResponse: {
            value: [
              {
                id: 1,
                createdAt: '2023-08-27T10:50:29.847Z',
                updatedAt: '2023-08-27T10:50:29.847Z',
                mark: 91,
                studentId: 1,
                courseId: 1,
                lectorId: 1,
              },
            ],
          },
          GetMarkForCourseResponse: {
            value: [{ mark: 91, studentName: 'loh', courseName: 'Python', lectorName: 'Some Guy' }],
          },
          GetMarkForStudentResponse: {
            value: [
              {
                courseName: 'Python',
                mark: 91,
              },
            ],
          },
        },
      },
    },
  })
  @Get()
  findAllMarks(
    @Query() marksPaginationQueryDto: MarksPaginationQueryDto,
  ): Promise<GetMarkResposeDto[] | GetMarkForCourseResponseDto[] | GetMarkForStudentResponseDto[]> {
    return this.marksService.findAllMarks(marksPaginationQueryDto);
  }

  @ApiOperation({ summary: 'Find a mark by id' })
  @ApiBadRequestResponse({
    description: 'Invalid id param.',
  })
  @ApiNotFoundResponse({ description: 'Mark with { id } not found.' })
  @ApiOkResponse({
    type: GetMarkResposeDto,
    description: 'A mark object.',
  })
  @Get(':id')
  findMarkById(@Param('id', ParseIntPipe) id: number) {
    return this.marksService.findMarkById(id);
  }

  @ApiOperation({ summary: 'Update a group by id' })
  @ApiBadRequestResponse({
    description: 'Invalid id, mark, studentId, courseId or lectorId. | Mark with { id } not found.',
  })
  @ApiOkResponse({
    type: GetMarkResposeDto,
    description: 'Updated mark object.',
  })
  @Patch(':id')
  updateMarkById(@Param('id', ParseIntPipe) id: number, @Body() updateMarkDto: UpdateMarkDto) {
    return this.marksService.updateMarkById(id, updateMarkDto);
  }

  @ApiOperation({ summary: 'Remove a mark by id' })
  @ApiBadRequestResponse({
    description: 'Invalid id param. | Mark with { id } not found.',
  })
  @ApiNoContentResponse({
    description: 'Mark successfully removed.',
  })
  @HttpCode(HttpStatus.NO_CONTENT)
  @Delete(':id')
  remove(@Param('id', ParseIntPipe) id: number) {
    return this.marksService.removeMarkbyId(id);
  }
}
