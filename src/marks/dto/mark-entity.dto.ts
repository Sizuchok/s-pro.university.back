import { ApiProperty } from '@nestjs/swagger';
import { Max, Min, IsInt, IsPositive, IsNumber } from 'class-validator';
import { CoreEntityDto } from '../../application/dto/core-entity.dto';

export class MarkEntityDto extends CoreEntityDto {
  @ApiProperty({ example: 90 })
  @Max(100)
  @Min(60)
  @IsInt()
  @IsPositive()
  @IsNumber()
  readonly mark: number;

  @ApiProperty({ example: 1 })
  @IsInt()
  @IsPositive()
  @IsNumber()
  readonly courseId: number;

  @ApiProperty({ example: 1 })
  @IsInt()
  @IsPositive()
  @IsNumber()
  readonly studentId: number;

  @ApiProperty({ example: 1 })
  @IsInt()
  @IsPositive()
  @IsNumber()
  readonly lectorId: number;
}
