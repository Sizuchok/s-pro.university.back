import { ApiPropertyOptional } from '@nestjs/swagger';
import { IsInt, IsNumber, IsOptional, IsPositive } from 'class-validator';

export class MarksPaginationQueryDto {
  @ApiPropertyOptional({
    description: 'A student`s id.',
    example: 5,
  })
  @IsOptional()
  @IsInt()
  @IsPositive()
  @IsNumber()
  studentId: number;

  @ApiPropertyOptional({
    description: 'A course id.',
    example: 7,
  })
  @IsOptional()
  @IsInt()
  @IsPositive()
  @IsNumber()
  courseId: number;
}
