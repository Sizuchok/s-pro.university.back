import { MarkEntityDto } from '../mark-entity.dto';
import { OmitType } from '@nestjs/swagger';

export class CreateMarkDto extends OmitType(MarkEntityDto, [
  'id',
  'createdAt',
  'updatedAt',
] as const) {}
