import { ApiProperty, PickType } from '@nestjs/swagger';
import { MarkEntityDto } from '../mark-entity.dto';
import { MaxLength, MinLength, IsString } from 'class-validator';

export class GetMarkForCourseResponseDto extends PickType(MarkEntityDto, ['mark'] as const) {
  @ApiProperty({
    example: 'John',
  })
  @MaxLength(16)
  @MinLength(2)
  @IsString()
  readonly studentName: string;

  @ApiProperty({
    example: 'Python',
  })
  @MaxLength(100)
  @MinLength(3)
  @IsString()
  readonly courseName: string;

  @ApiProperty({ example: 'Michael' })
  @MaxLength(16)
  @MinLength(2)
  @IsString()
  readonly lectorName: string;
}
