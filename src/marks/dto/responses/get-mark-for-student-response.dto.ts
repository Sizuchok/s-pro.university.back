import { PickType } from '@nestjs/swagger';
import { GetMarkForCourseResponseDto } from './get-mark-for-course-response.dto';

export class GetMarkForStudentResponseDto extends PickType(GetMarkForCourseResponseDto, [
  'mark',
  'courseName',
] as const) {}
