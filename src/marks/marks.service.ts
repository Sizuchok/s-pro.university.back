import {
  BadRequestException,
  Injectable,
  InternalServerErrorException,
  NotFoundException,
} from '@nestjs/common';
import { CreateMarkDto } from './dto/requests/create-mark.dto';
import { UpdateMarkDto } from './dto/requests/update-mark.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { Mark } from './entities/mark.entity';
import { Repository } from 'typeorm';
import { MarksPaginationQueryDto } from './dto/marks-pagination-query.dto.ts';
import { CreateMarkResponseDto } from './dto/responses/create-mark-response.dto';
import { GetMarkForCourseResponseDto } from './dto/responses/get-mark-for-course-response.dto';
import { GetMarkForStudentResponseDto } from './dto/responses/get-mark-for-student-response.dto';
import { GetMarkResposeDto } from './dto/responses/get-mark-response.dto';
import { Course } from '../courses/entities/course.entity';
import { Lector } from '../lectors/entities/lector.entity';
import { Student } from '../students/entities/student.entity';

@Injectable()
export class MarksService {
  constructor(
    @InjectRepository(Mark) private marksRepository: Repository<Mark>,
    @InjectRepository(Student) private studentsRepository: Repository<Student>,
    @InjectRepository(Course) private coursesRepository: Repository<Course>,
    @InjectRepository(Lector) private lectorsRepository: Repository<Lector>,
  ) {}

  async createMark({
    mark,
    studentId,
    courseId,
    lectorId,
  }: CreateMarkDto): Promise<CreateMarkResponseDto> {
    const student = await this.studentsRepository.findOneBy({ id: studentId });

    if (!student) throw new BadRequestException(`Student with { id: ${studentId} } not found.`);

    const course = await this.coursesRepository.findOneBy({ id: courseId });

    if (!course) throw new BadRequestException(`Course with { id: ${courseId} } not found.`);

    const lector = await this.lectorsRepository.findOneBy({
      id: lectorId,
      lectorCourses: {
        course: {
          id: courseId,
        },
      },
    });

    if (!lector)
      throw new BadRequestException(
        `Lector { id: ${lectorId} } associated with ${course.name} course not found.`,
      );

    return this.marksRepository.save({ mark, studentId, courseId, lectorId });
  }

  findAllMarks({
    studentId,
    courseId,
  }: MarksPaginationQueryDto): Promise<
    GetMarkResposeDto[] | GetMarkForCourseResponseDto[] | GetMarkForStudentResponseDto[]
  > {
    if (studentId) return this.getMarksForStudentById(studentId);
    else if (courseId) return this.getMarksForCourseById(courseId);

    return this.marksRepository.find();
  }

  private getMarksForStudentById = async (id: number): Promise<GetMarkForStudentResponseDto[]> => {
    const student = await this.studentsRepository.findOneBy({ id });

    if (!student) throw new BadRequestException(`Student with { id: ${id} } not found.`);

    const studentMarks: GetMarkForStudentResponseDto[] = await this.marksRepository
      .createQueryBuilder('mark')
      .select(['course.name as "courseName"', 'mark.mark as mark'])
      .leftJoin('mark.course', 'course')
      .where('mark.student_id = :id', { id })
      .getRawMany();

    return studentMarks;
  };

  private getMarksForCourseById = async (id: number): Promise<GetMarkForCourseResponseDto[]> => {
    const course = await this.coursesRepository.findOneBy({ id });

    if (!course) throw new BadRequestException(`Course with { id: ${id} } not found`);

    const marks: GetMarkForCourseResponseDto[] = await this.marksRepository
      .createQueryBuilder('mark')
      .select([
        'mark.mark as mark',
        'student.name as "studentName"',
        'course.name as "courseName"',
        'lector.name as "lectorName"',
      ])
      .leftJoin('mark.student', 'student')
      .leftJoin('mark.course', 'course')
      .leftJoin('mark.lector', 'lector')
      .where('mark.course_id = :id', { id })
      .getRawMany();

    return marks;
  };

  async findMarkById(id: number): Promise<GetMarkResposeDto> {
    const mark = await this.marksRepository.findOneBy({ id });

    if (!mark) throw new NotFoundException(`Mark with { id: ${id} } not found.`);

    return mark;
  }

  async updateMarkById(id: number, updateMarkDto: UpdateMarkDto): Promise<GetMarkResposeDto> {
    const mark = await this.marksRepository.findOneBy({ id });

    if (!mark) throw new BadRequestException(`Mark with { id: ${id} } not found for update.`);

    const result = await this.marksRepository.update(id, updateMarkDto);

    if (!result.affected)
      throw new InternalServerErrorException('The mark was not updated for some reason.');

    const updatedMark = await this.marksRepository.findOneBy({ id });

    if (!updatedMark)
      throw new InternalServerErrorException('Error retrieving the updated mark after update.');

    return updatedMark;
  }

  async removeMarkbyId(id: number): Promise<void> {
    const mark = await this.marksRepository.findOneBy({ id });

    if (!mark) throw new BadRequestException(`Mark with { id: ${id} } not found for deletion.`);

    const result = await this.marksRepository.delete(id);

    if (!result.affected) throw new InternalServerErrorException('Internal server error.');
  }
}
