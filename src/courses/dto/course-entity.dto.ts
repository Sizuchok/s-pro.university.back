import { ApiProperty } from '@nestjs/swagger';
import { MaxLength, MinLength, IsString, Max, IsInt, IsPositive, IsNumber } from 'class-validator';
import { CoreEntityDto } from '../../application/dto/core-entity.dto';

export class CourseEntityDto extends CoreEntityDto {
  @ApiProperty({
    example: 'Python',
  })
  @MaxLength(100)
  @MinLength(3)
  @IsString()
  readonly name: string;

  @ApiProperty({ example: 'A course about basics of python programming language.' })
  @MaxLength(200)
  @IsString()
  readonly description: string;

  @ApiProperty({
    example: 40,
  })
  @Max(100)
  @IsInt()
  @IsPositive()
  @IsNumber()
  readonly hours: number;
}
