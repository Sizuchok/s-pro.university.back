import { ApiProperty } from '@nestjs/swagger';
import { GetCourseResponseDto } from './get-course-response.dto';

export class GetAllCoursesWithStudentsCountAndCount {
  @ApiProperty()
  readonly courses: GetCourseResponseDto[];

  @ApiProperty({
    example: 1,
  })
  readonly count: number;
}
