import { CourseEntityDto } from '../course-entity.dto';

export class GetCourseResponseDto extends CourseEntityDto {}
