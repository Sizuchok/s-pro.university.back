import { IsEnum, IsInt, IsNumber, IsOptional, IsPositive } from 'class-validator';
import { BasePaginationQuery } from '../../application/dto/base-pagination-query.dto';
import { ApiPropertyOptional } from '@nestjs/swagger';
import { BaseSortBy } from '../../application/dto/enums/base-query.enums';

export const CoursesSortby = {
  ...BaseSortBy,
  name: 'name',
} as const;

export class CoursesPaginationQueryDto extends BasePaginationQuery {
  @ApiPropertyOptional({
    description: 'A lectorId query param',
    example: '5',
  })
  @IsOptional()
  @IsInt()
  @IsPositive()
  @IsNumber()
  lectorId: number;

  @ApiPropertyOptional({
    description: 'A field by which courses list should be sorted',
    example: 'name',
  })
  @IsOptional()
  @IsEnum(CoursesSortby, { message: 'Invalid sortBy parameter' })
  readonly sortBy: string;
}
