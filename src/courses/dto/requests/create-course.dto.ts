import { OmitType } from '@nestjs/swagger';
import { CourseEntityDto } from '../course-entity.dto';

export class CreateCourseDto extends OmitType(CourseEntityDto, [
  'id',
  'createdAt',
  'updatedAt',
] as const) {}
