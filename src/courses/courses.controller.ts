import {
  Controller,
  Get,
  Post,
  Body,
  Query,
  HttpStatus,
  Delete,
  Param,
  ParseIntPipe,
  Patch,
  HttpCode,
} from '@nestjs/common';
import { CoursesService } from './courses.service';
import { CreateCourseDto } from './dto/requests/create-course.dto';
import { CoursesPaginationQueryDto } from './dto/courses-pagination-query.dto';
import {
  ApiBadRequestResponse,
  ApiBearerAuth,
  ApiCreatedResponse,
  ApiInternalServerErrorResponse,
  ApiNoContentResponse,
  ApiNotFoundResponse,
  ApiOkResponse,
  ApiOperation,
  ApiTags,
} from '@nestjs/swagger';
import { GetCourseResponseDto } from './dto/responses/get-course-response.dto';

import { CourseEntityDto } from './dto/course-entity.dto';
import { InternalServerErrorResponse } from '../application/dto/internal-server-error-response.dto';
import { UpdateCourseDto } from './dto/requests/update-course.dto';
import { GetAllCoursesWithStudentsCountAndCount } from './dto/responses/get-all-courses-with-students-count-and-count.dto';

@ApiBearerAuth()
@ApiInternalServerErrorResponse({
  description: 'Internal server error',
  type: InternalServerErrorResponse,
})
@ApiTags('courses')
@Controller('courses')
export class CoursesController {
  constructor(private readonly coursesService: CoursesService) {}

  @ApiOperation({ summary: 'Create a course' })
  @ApiBadRequestResponse({
    description: 'Invalid name, description or hours',
  })
  @ApiCreatedResponse({
    type: GetCourseResponseDto,
    description: 'A freshly created course object.',
  })
  @Post()
  createCourse(@Body() createCourseDto: CreateCourseDto): Promise<CourseEntityDto> {
    return this.coursesService.createCourse(createCourseDto);
  }

  @ApiOperation({ summary: 'Find all courses' })
  @ApiBadRequestResponse({
    description:
      'Invalid pagination query params, either: query, sortBy, order, limit, offset or lectorId params',
  })
  @ApiNotFoundResponse({ description: 'Course with { id } not found.' })
  @ApiOkResponse({
    type: GetAllCoursesWithStudentsCountAndCount,
    isArray: true,
    description: 'An array of course objects.',
  })
  @Get()
  findAllCourses(
    @Query() coursesPaginationQuery: CoursesPaginationQueryDto,
  ): Promise<GetAllCoursesWithStudentsCountAndCount> {
    return this.coursesService.findAllCoursesWithStudentsCount(coursesPaginationQuery);
  }

  @ApiOperation({ summary: 'Find a course by id' })
  @ApiNotFoundResponse({ description: 'Course with { id } not found.' })
  @ApiBadRequestResponse({
    description: 'Invalid id param.',
  })
  @ApiOkResponse({
    type: GetCourseResponseDto,
    description: 'A course object.',
  })
  @Get(':id')
  findCourseById(@Param('id', ParseIntPipe) id: number) {
    return this.coursesService.findCourseById(id);
  }

  @ApiOperation({ summary: 'Update a course by id' })
  @ApiBadRequestResponse({
    description: 'Invalid id param | Invalid course update data. | Course with { id } not found.',
  })
  @ApiOkResponse({
    type: GetCourseResponseDto,
    description: 'Updated course object.',
  })
  @Patch(':id')
  updateCourseById(
    @Param('id', ParseIntPipe) id: number,
    @Body() updateCourseDto: UpdateCourseDto,
  ) {
    return this.coursesService.updateCourseById(id, updateCourseDto);
  }

  @ApiOperation({ summary: 'Remove a course by id' })
  @ApiBadRequestResponse({
    description: 'Invalid id param. | Course with { id } not found.',
  })
  @ApiNoContentResponse({
    description: 'Course successfully removed.',
  })
  @HttpCode(HttpStatus.NO_CONTENT)
  @Delete(':id')
  removeCourseById(@Param('id', ParseIntPipe) id: number) {
    return this.coursesService.removeCourseById(id);
  }
}
