import {
  BadRequestException,
  Injectable,
  InternalServerErrorException,
  NotFoundException,
} from '@nestjs/common';
import { CreateCourseDto } from './dto/requests/create-course.dto';
import { UpdateCourseDto } from './dto/requests/update-course.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { FindManyOptions, ILike, Repository } from 'typeorm';
import { Course } from './entities/course.entity';
import { CoursesPaginationQueryDto } from './dto/courses-pagination-query.dto';
import { CourseEntityDto } from './dto/course-entity.dto';
import { GetCourseResponseDto } from './dto/responses/get-course-response.dto';
import { GetAllCoursesWithStudentsCountAndCount } from './dto/responses/get-all-courses-with-students-count-and-count.dto';

@Injectable()
export class CoursesService {
  constructor(@InjectRepository(Course) private coursesRepository: Repository<Course>) {}

  createCourse(createCourseDto: CreateCourseDto): Promise<CourseEntityDto> {
    return this.coursesRepository.save(createCourseDto);
  }

  findAllCourses({
    lectorId,
    limit,
    offset,
    order,
    query,
    sortBy,
  }: CoursesPaginationQueryDto): Promise<GetCourseResponseDto[]> {
    const findOptions: FindManyOptions<Course> = {
      skip: offset,
      take: limit,
    };

    if (lectorId)
      findOptions.where = {
        lectorCourses: {
          lector: {
            id: lectorId,
          },
        },
      };

    if (query)
      findOptions.where = [{ name: ILike(`%${query}%`) }, { description: ILike(`%${query}%`) }];

    if (sortBy) findOptions.order = { [sortBy]: order };

    return this.coursesRepository.find(findOptions);
  }

  async findAllCoursesWithStudentsCount({
    lectorId,
    limit,
    offset,
    order,
    query,
    sortBy,
  }: CoursesPaginationQueryDto): Promise<GetAllCoursesWithStudentsCountAndCount> {
    const queryBuilder = this.coursesRepository.createQueryBuilder('course');

    queryBuilder.select([
      'course.id as id',
      'course.name as name',
      'course.description as description',
      'course.hours as hours',
      'course.createdAt as "createdAt"',
      'course.updatedAt as "updatedAt"',
    ]);

    if (lectorId) {
      queryBuilder.where('course.lectorId = :lectorId', { lectorId });
    }

    if (query) {
      queryBuilder.where('(course.name ILIKE :query OR course.description ILIKE :query)', {
        query: `%${query}%`,
      });
    }

    if (sortBy && order) {
      queryBuilder.orderBy(`course.${sortBy}`, order === 'asc' ? 'ASC' : 'DESC');
    }

    queryBuilder.skip(offset);
    queryBuilder.take(limit);

    // Use a subquery to count the students in each course
    queryBuilder.addSelect(subQuery => {
      return subQuery
        .select('COUNT(student_course.student_id)', 'count')
        .from('student_course', 'student_course')
        .where('student_course.course_id = course.id');
    }, 'studentsCount');

    const courses = await queryBuilder.getRawMany();

    const formattedCourses = courses.map(({ studentsCount, ...data }) => ({
      ...data,
      studentsCount: Number(studentsCount),
    }));

    return {
      courses: formattedCourses,
      count: formattedCourses.length,
    };
  }

  async findCourseById(id: number): Promise<GetCourseResponseDto> {
    const lector = await this.coursesRepository.findOneBy({ id });

    if (!lector) throw new NotFoundException(`Course with { id: ${id} } not found.`);

    return lector;
  }

  async updateCourseById(
    id: number,
    updateCourseDto: UpdateCourseDto,
  ): Promise<GetCourseResponseDto> {
    const course = await this.coursesRepository.findOneBy({ id });

    if (!course) throw new BadRequestException(`Course with { id: ${id} } not found for update.`);

    const result = await this.coursesRepository.update(id, updateCourseDto);

    if (!result.affected)
      throw new InternalServerErrorException('Course was not updated for some reason.');

    const updatedCourse = await this.coursesRepository.findOneBy({ id });

    if (!updatedCourse)
      throw new InternalServerErrorException('Error retrieving the updated course after update.');

    return updatedCourse;
  }

  async removeCourseById(id: number): Promise<void> {
    const course = await this.coursesRepository.findOneBy({ id });

    if (!course) throw new BadRequestException(`Course with { id: ${id} } not found for deletion.`);

    const result = await this.coursesRepository.delete(id);

    if (!result.affected) throw new InternalServerErrorException('Internal server error.');
  }
}
