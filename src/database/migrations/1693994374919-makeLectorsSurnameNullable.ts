import { MigrationInterface, QueryRunner } from 'typeorm';

export class MakeLectorsSurnameNullable1693994374919 implements MigrationInterface {
  name = 'MakeLectorsSurnameNullable1693994374919';

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`ALTER TABLE "lectors" ALTER COLUMN "surname" DROP NOT NULL`);
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`ALTER TABLE "lectors" ALTER COLUMN "surname" SET NOT NULL`);
  }
}
