import { MigrationInterface, QueryRunner } from 'typeorm';

export class MakeLectorIdInMarksTableNullabe1693695053899 implements MigrationInterface {
  name = 'MakeLectorIdInMarksTableNullabe1693695053899';

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`ALTER TABLE "marks" DROP CONSTRAINT "FK_69b5af3347a46bb74ccc3c6f65c"`);
    await queryRunner.query(`ALTER TABLE "marks" DROP CONSTRAINT "UQ_d9b96371110f5b449c2fada769c"`);
    await queryRunner.query(`ALTER TABLE "marks" ALTER COLUMN "lector_id" DROP NOT NULL`);
    await queryRunner.query(
      `ALTER TABLE "marks" ADD CONSTRAINT "UQ_d9b96371110f5b449c2fada769c" UNIQUE ("course_id", "student_id", "lector_id")`,
    );
    await queryRunner.query(
      `ALTER TABLE "marks" ADD CONSTRAINT "FK_69b5af3347a46bb74ccc3c6f65c" FOREIGN KEY ("lector_id") REFERENCES "lectors"("id") ON DELETE SET NULL ON UPDATE NO ACTION`,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`ALTER TABLE "marks" DROP CONSTRAINT "FK_69b5af3347a46bb74ccc3c6f65c"`);
    await queryRunner.query(`ALTER TABLE "marks" DROP CONSTRAINT "UQ_d9b96371110f5b449c2fada769c"`);
    await queryRunner.query(`ALTER TABLE "marks" ALTER COLUMN "lector_id" SET NOT NULL`);
    await queryRunner.query(
      `ALTER TABLE "marks" ADD CONSTRAINT "UQ_d9b96371110f5b449c2fada769c" UNIQUE ("student_id", "course_id", "lector_id")`,
    );
    await queryRunner.query(
      `ALTER TABLE "marks" ADD CONSTRAINT "FK_69b5af3347a46bb74ccc3c6f65c" FOREIGN KEY ("lector_id") REFERENCES "lectors"("id") ON DELETE SET NULL ON UPDATE NO ACTION`,
    );
  }
}
