import { MigrationInterface, QueryRunner } from 'typeorm';

export class AddSurnameToLectorEntity1693669417159 implements MigrationInterface {
  name = 'AddSurnameToLectorEntity1693669417159';

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`ALTER TABLE "lectors" ADD "surname" character varying NOT NULL`);
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`ALTER TABLE "lectors" DROP COLUMN "surname"`);
  }
}
