import { MigrationInterface, QueryRunner } from 'typeorm';

export class AddOnDeleteCascadeConstraintToResetTokenEntity1697809314047
  implements MigrationInterface
{
  name = 'AddOnDeleteCascadeConstraintToResetTokenEntity1697809314047';

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `ALTER TABLE "reset_tokens" DROP CONSTRAINT "FK_c3d46e6cc811d3ac7df4bfb9c86"`,
    );
    await queryRunner.query(
      `ALTER TABLE "reset_tokens" ADD CONSTRAINT "FK_c3d46e6cc811d3ac7df4bfb9c86" FOREIGN KEY ("lector_id") REFERENCES "lectors"("id") ON DELETE CASCADE ON UPDATE NO ACTION`,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `ALTER TABLE "reset_tokens" DROP CONSTRAINT "FK_c3d46e6cc811d3ac7df4bfb9c86"`,
    );
    await queryRunner.query(
      `ALTER TABLE "reset_tokens" ADD CONSTRAINT "FK_c3d46e6cc811d3ac7df4bfb9c86" FOREIGN KEY ("lector_id") REFERENCES "lectors"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`,
    );
  }
}
