import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  HttpStatus,
  UseInterceptors,
  ClassSerializerInterceptor,
  Query,
  HttpCode,
  ParseIntPipe,
} from '@nestjs/common';
import { LectorsService } from './lectors.service';
import { CreateLectorDto } from './dto/requests/create-lector.dto';
import { AssociateLectorWithCourseDto } from './dto/requests/associate-lector-with-course.dto';
import {
  ApiBadRequestResponse,
  ApiBearerAuth,
  ApiConflictResponse,
  ApiCreatedResponse,
  ApiInternalServerErrorResponse,
  ApiNoContentResponse,
  ApiNotFoundResponse,
  ApiOkResponse,
  ApiOperation,
  ApiResponse,
  ApiTags,
} from '@nestjs/swagger';
import { CreateLectorResponseDto } from './dto/responses/create-lector-response.dto';
import { GetLectorWithCoursesResponse } from './dto/responses/get-lector-with-courses-response.dto';
import { AssociateLectorWithCourseResponseDto } from './dto/responses/associate-lector-with-course-response.dto';
import { InternalServerErrorResponse } from '../application/dto/internal-server-error-response.dto';
import { CurrentUser } from '../auth/decorators/current-user.decorator';
import { LectorMe } from './types/LectorMe';
import { UpdateLectorDto } from './dto/requests/update-lector.dto';
import { GetLectorMeResponseDto } from './dto/responses/get-lector-me-response.dto';
import { LectorsPaginationsQueryDto } from './dto/lectors-pagination-query.dto';
import { GetLectorWithoutPasswordDto } from './dto/responses/get-lector-without-password.dto';
import { GetAllLectorsWithCount } from './dto/responses/get-all-lectors-with-count.dto';

@ApiBearerAuth()
@ApiInternalServerErrorResponse({
  description: 'Internal server error',
  type: InternalServerErrorResponse,
})
@ApiTags('lectors')
@UseInterceptors(ClassSerializerInterceptor)
@Controller('lectors')
export class LectorsController {
  constructor(private readonly lectorsService: LectorsService) {}

  @ApiOperation({ summary: 'Create a lector' })
  @ApiConflictResponse({
    description: 'This email is already associated with an account.',
  })
  @ApiBadRequestResponse({
    description: 'Invalid name, surname, email or password',
  })
  @ApiCreatedResponse({
    type: CreateLectorResponseDto,
    description: 'A freshly created lector object without password.',
  })
  @Post()
  createLector(@Body() createLectorDto: CreateLectorDto): Promise<CreateLectorResponseDto> {
    return this.lectorsService.createLector(createLectorDto);
  }

  @ApiOperation({ summary: 'Find an authenticated lector by id' })
  @ApiBadRequestResponse({
    description: 'Invalid id param',
  })
  @ApiNotFoundResponse({ description: 'Lector with { id } not found.' })
  @ApiOkResponse({
    type: GetLectorWithoutPasswordDto,
    description: 'A lector object without password.',
  })
  @Get('me')
  public async findMe(@CurrentUser() lector: LectorMe): Promise<GetLectorMeResponseDto> {
    return this.lectorsService.findLectorById(lector.sub);
  }

  @ApiOperation({ summary: 'Find all lectors' })
  @ApiOkResponse({
    type: GetAllLectorsWithCount,
    isArray: true,
    description: 'An array of lectors.',
  })
  @Get()
  findAllLectors(
    @Query()
    lectorsPaginationsQueryDto: LectorsPaginationsQueryDto,
  ): Promise<GetAllLectorsWithCount> {
    return this.lectorsService.findAllLectos(lectorsPaginationsQueryDto);
  }

  @ApiOperation({ summary: 'Find a lector with his courses by id' })
  @ApiBadRequestResponse({
    description: 'Invalid id param',
  })
  @ApiNotFoundResponse({ description: 'Lector with { id } not found.' })
  @ApiResponse({
    status: HttpStatus.OK,
    type: GetLectorWithCoursesResponse,
    description: 'A lector object with its courses',
  })
  @Get(':id/courses')
  async findLectorWithCoursesById(
    @Param('id', ParseIntPipe) id: number,
  ): Promise<GetLectorWithCoursesResponse> {
    return this.lectorsService.findLectorWithCoursesById(id);
  }

  @ApiOperation({ summary: 'Find a lector by id' })
  @ApiBadRequestResponse({
    description: 'Invalid id param',
  })
  @ApiNotFoundResponse({ description: 'Lector with { id } not found.' })
  @ApiOkResponse({
    type: GetLectorWithoutPasswordDto,
    description: 'A lector object without password.',
  })
  @Get(':id')
  async findLectorById(@Param('id', ParseIntPipe) id: number): Promise<GetLectorMeResponseDto> {
    return this.lectorsService.findLectorById(id);
  }

  @ApiOperation({ summary: 'Update a lector by id' })
  @ApiBadRequestResponse({
    description: 'Invalid id, name, surname, email or password. | Lector with { id } not found.',
  })
  @ApiResponse({
    status: HttpStatus.OK,
    type: GetLectorWithoutPasswordDto,
    description: 'A lector object without password.',
  })
  @Patch(':id')
  async updateLectorById(
    @Param('id', ParseIntPipe) id: number,
    @Body() updateLectorDto: UpdateLectorDto,
  ): Promise<GetLectorMeResponseDto> {
    console.log(updateLectorDto);
    return this.lectorsService.updateLectorById(id, updateLectorDto);
  }

  @ApiOperation({ summary: 'Associate a lector with a course' })
  @ApiBadRequestResponse({
    description:
      'Invalid id, name, surname, email or password. | Lector with { id } not found. | Course with { id } not found.',
  })
  @ApiResponse({
    status: HttpStatus.CREATED,
    type: AssociateLectorWithCourseResponseDto,
    description: 'A lector object.',
  })
  @Post(':id/course')
  associateLectorWithCourse(
    @Param('id', ParseIntPipe) id: number,
    @Body() { courseId }: AssociateLectorWithCourseDto,
  ): Promise<AssociateLectorWithCourseResponseDto> {
    return this.lectorsService.associateLectorWithCourse(id, courseId);
  }

  @ApiOperation({ summary: 'Remove a lector by id' })
  @ApiBadRequestResponse({
    description: 'Invalid id param. | Lector with { id } not found.',
  })
  @ApiNoContentResponse({
    description: 'Student successfully removed.',
  })
  @HttpCode(HttpStatus.NO_CONTENT)
  @Delete(':id')
  removeLectorById(@Param('id', ParseIntPipe) id: number): Promise<void> {
    return this.lectorsService.removeLectorById(id);
  }
}
