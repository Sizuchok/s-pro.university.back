import { Module } from '@nestjs/common';
import { LectorsService } from './lectors.service';
import { LectorsController } from './lectors.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Lector } from './entities/lector.entity';
import { Course } from '../courses/entities/course.entity';
import { HashingModule } from '../hashing/hashing.module';

@Module({
  imports: [TypeOrmModule.forFeature([Lector, Course]), HashingModule],
  controllers: [LectorsController],
  providers: [LectorsService],
  exports: [LectorsService],
})
export class LectorsModule {}
