export type LectorMe = {
  sub: number;
  email: string;
};
