import { Exclude } from 'class-transformer';
import { LectorEntityDto } from '../lector-entity.dto';

export class GetLectorMeResponseDto extends LectorEntityDto {
  constructor(lectorEntity: LectorEntityDto) {
    super();
    Object.assign(this, lectorEntity);
  }

  @Exclude()
  password: string;
}
