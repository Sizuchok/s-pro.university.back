import { ApiProperty } from '@nestjs/swagger';
import { LectorEntityDto } from '../lector-entity.dto';
import { CoreEntityDto } from '../../../application/dto/core-entity.dto';
import { CourseEntityDto } from '../../../courses/dto/course-entity.dto';
import { GetLectorWithoutPasswordDto } from './get-lector-without-password.dto';

export class AssociateLectorWithCourseResponseDto extends CoreEntityDto {
  @ApiProperty({
    type: CourseEntityDto,
  })
  readonly course: CourseEntityDto;

  @ApiProperty({
    type: GetLectorWithoutPasswordDto,
  })
  readonly lector: LectorEntityDto;
}
