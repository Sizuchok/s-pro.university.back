import { OmitType } from '@nestjs/swagger';
import { LectorEntityDto } from '../lector-entity.dto';

export class CreateLectorResponseDto extends OmitType(LectorEntityDto, ['password'] as const) {}
