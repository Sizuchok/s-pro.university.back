import { ApiProperty } from '@nestjs/swagger';
import { GetLectorResponseDto } from './get-lector-response.dto';

export class GetAllLectorsWithCount {
  @ApiProperty()
  readonly lectors: GetLectorResponseDto[];

  @ApiProperty({
    example: 1,
  })
  readonly count: number;
}
