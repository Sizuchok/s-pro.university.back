import { ApiProperty } from '@nestjs/swagger';
import { LectorEntityDto } from '../lector-entity.dto';

export class GetLectorResponseDto extends LectorEntityDto {
  @ApiProperty({ example: '$2b$10$VMqCdzAuzINMbOz/Af9g0OdaZtHZIkFaA6nUnjcJfX7F9oYjDHrVK' })
  readonly password: string;
}
