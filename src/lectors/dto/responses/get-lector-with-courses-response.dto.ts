import { CourseEntityDto } from '../../../courses/dto/course-entity.dto';
import { LectorEntityDto } from '../lector-entity.dto';
import { ApiProperty, OmitType } from '@nestjs/swagger';

export class GetLectorWithCoursesResponse extends OmitType(LectorEntityDto, ['password'] as const) {
  @ApiProperty({
    type: CourseEntityDto,
    isArray: true,
  })
  readonly courses: CourseEntityDto[];
}
