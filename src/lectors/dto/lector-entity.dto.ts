import { ApiProperty } from '@nestjs/swagger';
import { MaxLength, MinLength, IsString, IsEmail, Matches } from 'class-validator';
import { CoreEntityDto } from '../../application/dto/core-entity.dto';

export class LectorEntityDto extends CoreEntityDto {
  @ApiProperty({ example: 'Michael' })
  @MaxLength(16)
  @MinLength(2)
  @IsString()
  readonly name: string;

  @ApiProperty({ example: 'Pitterson' })
  @MaxLength(25)
  @MinLength(2)
  @IsString()
  readonly surname: string;

  @ApiProperty({ example: 'michael@gmail.com', format: 'email' })
  @IsEmail()
  @IsString()
  readonly email: string;

  @ApiProperty({ example: 'm1CH@e1001' })
  @MaxLength(24)
  @MinLength(8)
  @IsString()
  @Matches(/(?=.*\d)(?=.*[a-z])(?=.*[A-Z])/, {
    message:
      'password must contain at least one uppercase letter, one lowercase letter, and one number',
  })
  readonly password: string;
}
