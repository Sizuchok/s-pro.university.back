import { ApiPropertyOptional } from '@nestjs/swagger';
import { IsOptional, IsEnum } from 'class-validator';
import { BasePaginationQuery } from '../../application/dto/base-pagination-query.dto';
import { BaseSortBy } from '../../application/dto/enums/base-query.enums';

export const LectorsSortBy = {
  ...BaseSortBy,
  name: 'name',
} as const;

export class LectorsPaginationsQueryDto extends BasePaginationQuery {
  @ApiPropertyOptional({
    description: 'A field by which lectors list should be sorted',
    example: 'createdAt',
  })
  @IsOptional()
  @IsEnum(LectorsSortBy, { message: 'Invalid sortBy parameter' })
  readonly sortBy: string;
}
