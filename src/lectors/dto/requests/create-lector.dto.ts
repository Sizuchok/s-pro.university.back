import { IntersectionType, PartialType, PickType } from '@nestjs/swagger';
import { LectorEntityDto } from '../lector-entity.dto';

export class CreateLectorDto extends IntersectionType(
  PickType(LectorEntityDto, ['name', 'email', 'password'] as const),
  PartialType(PickType(LectorEntityDto, ['surname'])),
) {}
