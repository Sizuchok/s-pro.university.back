import { IsInt, IsNumber, IsPositive } from 'class-validator';

export class AssociateLectorWithCourseDto {
  @IsPositive()
  @IsInt()
  @IsNumber()
  readonly courseId: number;
}
