import {
  BadRequestException,
  ConflictException,
  Injectable,
  InternalServerErrorException,
  NotFoundException,
} from '@nestjs/common';
import { CreateLectorDto } from './dto/requests/create-lector.dto';
import { UpdateLectorDto } from './dto/requests/update-lector.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { Lector } from './entities/lector.entity';
import { FindManyOptions, ILike, Repository } from 'typeorm';
import { AssociateLectorWithCourseResponseDto } from './dto/responses/associate-lector-with-course-response.dto';
import { GetLectorWithCoursesResponse } from './dto/responses/get-lector-with-courses-response.dto';
import { Course } from '../courses/entities/course.entity';
import { HashingService } from '../hashing/hashing.service';
import { LectorCourse } from '../lectors_courses/entities/lector-course.entity';
import { GetLectorMeResponseDto } from './dto/responses/get-lector-me-response.dto';
import { LectorsPaginationsQueryDto } from './dto/lectors-pagination-query.dto';
import { GetAllLectorsWithCount } from './dto/responses/get-all-lectors-with-count.dto';

@Injectable()
export class LectorsService {
  constructor(
    @InjectRepository(Lector) private lectorsRepository: Repository<Lector>,
    @InjectRepository(Course) private coursesRepository: Repository<Course>,
    private readonly hashingService: HashingService,
  ) {}

  async createLector(createLectorDto: CreateLectorDto): Promise<GetLectorMeResponseDto> {
    const lector = await this.findLectorByEmail(createLectorDto.email);
    if (lector) throw new ConflictException('This email is already associated with an account.');

    const password = await this.hashingService.hash(createLectorDto.password);

    const newLector = await this.lectorsRepository.save({ ...createLectorDto, password });

    return new GetLectorMeResponseDto({ ...newLector });
  }

  async findAllLectos({
    sortBy,
    order,
    limit,
    offset,
    query,
  }: LectorsPaginationsQueryDto): Promise<GetAllLectorsWithCount> {
    const findOptions: FindManyOptions<Lector> = {
      skip: offset,
      take: limit,
    };

    if (query)
      findOptions.where = [
        { name: ILike(`%${query}%`) },
        { surname: ILike(`%${query}%`) },
        { email: ILike(`%${query}%`) },
      ];

    if (sortBy) findOptions.order = { [sortBy]: order };

    return {
      lectors: await this.lectorsRepository.find(findOptions),
      count: await this.lectorsRepository.count(findOptions),
    };
  }

  async findLectorById(id: number): Promise<GetLectorMeResponseDto> {
    const lector = await this.lectorsRepository.findOneBy({ id });

    if (!lector) throw new NotFoundException(`Lector with { id: ${id} } not found.`);

    return new GetLectorMeResponseDto({ ...lector });
  }

  async findLectorWithCoursesById(id: number): Promise<GetLectorWithCoursesResponse> {
    const lector = await this.lectorsRepository.findOne({
      where: { id },
      relations: {
        lectorCourses: {
          course: true,
        },
      },
    });

    if (!lector) throw new NotFoundException(`Lector with { id: ${id} } not found.`);

    const { lectorCourses, ...rest } = lector;
    const courses = lectorCourses.map(({ course }) => course);
    const formattedLector = { ...rest, courses };

    return formattedLector;
  }

  async findLectorByEmail(email: string): Promise<GetLectorMeResponseDto> {
    return this.lectorsRepository.findOneBy({ email });
  }

  async updateLectorById(
    id: number,
    updateLectorDto: UpdateLectorDto,
  ): Promise<GetLectorMeResponseDto> {
    const lector = await this.lectorsRepository.findOneBy({ id });

    if (!lector) throw new BadRequestException(`Lector with { id: ${id} } not found for update.`);

    const updatedLectorDtoHashed = { ...updateLectorDto };

    if (updateLectorDto.password) {
      const hasedPassword = await this.hashingService.hash(updateLectorDto.password);
      updatedLectorDtoHashed.password = hasedPassword;
    }

    const result = await this.lectorsRepository.update(id, updatedLectorDtoHashed);

    if (!result.affected)
      throw new InternalServerErrorException('The lector was not updated for some reason.');

    const updatedLector = await this.lectorsRepository.findOneBy({ id });

    if (!updatedLector)
      throw new InternalServerErrorException('Error retrieving the updated lector after update.');

    return new GetLectorMeResponseDto({ ...updatedLector });
  }

  async updatePassword(id: number, newPassword: string): Promise<void> {
    const password = await this.hashingService.hash(newPassword);
    await this.lectorsRepository.update(id, { password });
  }

  async associateLectorWithCourse(
    lectorId: number,
    courseId: number,
  ): Promise<AssociateLectorWithCourseResponseDto> {
    const lector = await this.lectorsRepository.findOneBy({ id: lectorId });

    if (!lector) throw new BadRequestException(`Lector with { id: ${lectorId} } not found.`);

    const course = await this.coursesRepository.findOneBy({ id: courseId });

    if (!course) throw new BadRequestException(`Course with { id: ${courseId} } not found.`);

    const lectorCourse = new LectorCourse();
    lectorCourse.course = course;
    lectorCourse.lector = lector;

    return lectorCourse.save();
  }

  async removeLectorById(id: number): Promise<void> {
    const student = await this.lectorsRepository.findOneBy({ id });

    if (!student)
      throw new BadRequestException(`Lector with { id: ${id} } not found for deletion.`);

    const result = await this.lectorsRepository.delete(id);

    if (!result.affected) throw new InternalServerErrorException('Internal server error.');
  }
}
