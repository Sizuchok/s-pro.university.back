import { MailerOptions } from '@nestjs-modules/mailer';
import { HandlebarsAdapter } from '@nestjs-modules/mailer/dist/adapters/handlebars.adapter';
import * as path from 'path';

export const mailerConfig: MailerOptions = {
  transport: {
    host: process.env.SMTP_HOST,
    port: process.env.SMTP_PORT,
    secure: false,
    auth: {
      user: process.env.SMTP_USER,
      pass: process.env.SMTP_PASSWORD,
    },
  },
  defaults: {
    from: process.env.SMTP_DEFAULT_FROM,
  },
  template: {
    dir: path.join(__dirname, '../../emailer/templates/'),
    adapter: new HandlebarsAdapter(),
    options: {
      strict: true,
    },
  },
};
