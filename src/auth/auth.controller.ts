import { Body, Controller, HttpCode, HttpStatus, Post } from '@nestjs/common';
import { AuthService } from './auth.service';
import { SignInRequestDto } from './dto/requests/sign-in.request.dto';
import { SignInResponseDto } from './dto/responses/sign-in.response.dto';
import { Public } from './decorators/public-route.decorator';
import {
  ApiBadRequestResponse,
  ApiConflictResponse,
  ApiCreatedResponse,
  ApiInternalServerErrorResponse,
  ApiNoContentResponse,
  ApiOkResponse,
  ApiOperation,
  ApiTags,
  ApiUnauthorizedResponse,
} from '@nestjs/swagger';
import { SignUpRequestDto } from './dto/requests/sign-up-request.dto';
import { ResetPasswordRequestDto } from './dto/requests/reset-password.request.dto';
import { ResetPasswordWithTokenRequestDto } from './dto/requests/reset-password-with-token.request.dto';
import { SignUpResponseDto } from './dto/responses/sign-up.response.dto';
import { InternalServerErrorResponse } from '../application/dto/internal-server-error-response.dto';

@Public()
@ApiInternalServerErrorResponse({
  description: 'Internal server error',
  type: InternalServerErrorResponse,
})
@ApiTags('auth')
@Controller('auth')
export class AuthController {
  constructor(private authService: AuthService) {}

  @ApiOperation({ summary: 'Sign in' })
  @ApiOkResponse({
    description: 'JWT access token.',
    type: SignInResponseDto,
  })
  @ApiUnauthorizedResponse({
    description: 'Unauthorized exception.',
  })
  @ApiBadRequestResponse({
    description: 'Invalid email or password.',
  })
  @HttpCode(HttpStatus.OK)
  @Post('sign-in')
  async signIn(@Body() signInDto: SignInRequestDto) {
    return await this.authService.signIn(signInDto);
  }

  @ApiOperation({ summary: 'Sign up' })
  @ApiCreatedResponse({
    description: 'JWT access token',
    type: SignUpResponseDto,
  })
  @ApiBadRequestResponse({
    description: 'Invalid name, email or password.',
  })
  @ApiConflictResponse({
    description: 'This email is already associated with an account.',
  })
  @Post('sign-up')
  async signUp(@Body() signUpDto: SignUpRequestDto) {
    return await this.authService.signUp(signUpDto);
  }

  @ApiOperation({ summary: 'Reset password request' })
  @ApiBadRequestResponse({
    description: 'Invalid email | Lector with specified email not found',
  })
  @ApiOkResponse({
    description: 'Reset password token is sent to an email the lector specified',
  })
  @HttpCode(HttpStatus.OK)
  @Post('reset-password-request')
  public async resetPasswordRequest(@Body() resetPasswordDto: ResetPasswordRequestDto) {
    await this.authService.resetPasswordRequest(resetPasswordDto.email);
  }

  @ApiOperation({ summary: 'Reset password' })
  @ApiBadRequestResponse({
    description: 'Invalid new password, reset token or id',
  })
  @ApiNoContentResponse({
    description: 'New password successfully saved',
  })
  @HttpCode(HttpStatus.NO_CONTENT)
  @Post('reset-password')
  public async resetPassword(
    @Body() resetPasswordWithTokenRequestDto: ResetPasswordWithTokenRequestDto,
  ) {
    await this.authService.resetPassword(resetPasswordWithTokenRequestDto);
  }
}
