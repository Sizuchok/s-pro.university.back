import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsString } from 'class-validator';

export class SignInResponseDto {
  @ApiProperty({
    example:
      'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjcsImVtYWlsIjoic2l6dWNob2swQGdtYWlsLmNvbSIsImlhdCI6MTY5MzQ4MTE3MiwiZXhwIjoxNjkzNDg0NzcyfQ.NVzXp7Wr1vv6ktY1JEZPvDHOhp5gQ4bruJNRHPvfm7E',
  })
  @IsNotEmpty()
  @IsString()
  accessToken: string;
}
