import { CreateLectorDto } from '../../../lectors/dto/requests/create-lector.dto';

export class SignUpRequestDto extends CreateLectorDto {}
