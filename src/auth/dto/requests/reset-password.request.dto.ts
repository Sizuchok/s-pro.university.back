import { PickType } from '@nestjs/swagger';
import { CreateLectorDto } from '../../../lectors/dto/requests/create-lector.dto';

export class ResetPasswordRequestDto extends PickType(CreateLectorDto, ['email'] as const) {}
