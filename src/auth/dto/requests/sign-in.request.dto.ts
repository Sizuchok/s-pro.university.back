import { ApiProperty, PickType } from '@nestjs/swagger';
import { IsString, MinLength } from 'class-validator';
import { LectorEntityDto } from '../../../lectors/dto/lector-entity.dto';

export class SignInRequestDto extends PickType(LectorEntityDto, ['email'] as const) {
  @ApiProperty({ example: 'm1CH@e1001' })
  @MinLength(8)
  @IsString()
  readonly password: string;
}
