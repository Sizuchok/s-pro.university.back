import { IsString, Matches, MaxLength, MinLength } from 'class-validator';
import { ApiProperty, PickType } from '@nestjs/swagger';
import { ResetTokenEntityDto } from '../../../reset-token/dto/reset-token-entity.dto';

export class ResetPasswordWithTokenRequestDto extends PickType(ResetTokenEntityDto, [
  'lectorId',
  'token',
] as const) {
  @ApiProperty({ example: 'n3wPa$$wOrd' })
  @MaxLength(24)
  @MinLength(8)
  @IsString()
  @Matches(/(?=.*\d)(?=.*[a-z])(?=.*[A-Z])/, {
    message:
      'password must contain at least one uppercase letter, one lowercase letter, and one number',
  })
  readonly newPassword: string;
}
