import { Module } from '@nestjs/common';
import { AuthService } from './auth.service';
import { AuthController } from './auth.controller';
import { JwtModule } from '@nestjs/jwt';
import { APP_GUARD } from '@nestjs/core';
import { AuthGuard } from './auth.guard';
import { LectorsModule } from '../lectors/lectors.module';
import { ResetTokenModule } from '../reset-token/reset-token.module';
import { HashingModule } from '../hashing/hashing.module';
import { EmailerModule } from '../emailer/emailer.module';

@Module({
  imports: [
    LectorsModule,
    JwtModule.register({
      global: true,
      secret: process.env.JWT_SECRET,
      signOptions: { expiresIn: '3600s' },
    }),
    ResetTokenModule,
    HashingModule,
    EmailerModule,
  ],
  controllers: [AuthController],
  providers: [{ provide: APP_GUARD, useClass: AuthGuard }, AuthService],
  exports: [AuthService],
})
export class AuthModule {}
