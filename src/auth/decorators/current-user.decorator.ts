import { ExecutionContext, createParamDecorator } from '@nestjs/common';
import { REQUEST_USER_KEY } from '../auth.constants';

export const CurrentUser = createParamDecorator((argument: unknown, ctx: ExecutionContext) => {
  const request = ctx.switchToHttp().getRequest();
  return request[REQUEST_USER_KEY];
});
