import { BadRequestException, Injectable, UnauthorizedException } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { SignInRequestDto } from './dto/requests/sign-in.request.dto';
import { ResetPasswordWithTokenRequestDto } from './dto/requests/reset-password-with-token.request.dto';
import { HashingService } from '../hashing/hashing.service';
import { CreateLectorDto } from '../lectors/dto/requests/create-lector.dto';
import { LectorsService } from '../lectors/lectors.service';
import { ResetTokenService } from '../reset-token/reset-token.service';
import { SignInResponseDto } from './dto/responses/sign-in.response.dto';
import { EmailerService } from '../emailer/emailer.service';

@Injectable()
export class AuthService {
  constructor(
    private lectorsService: LectorsService,
    private jwtService: JwtService,
    private resetTokenService: ResetTokenService,
    private hashingService: HashingService,
    private mailerService: EmailerService,
  ) {}

  public async signUp(createLectorDto: CreateLectorDto): Promise<SignInResponseDto> {
    const lector = await this.lectorsService.createLector(createLectorDto);
    const payload = { sub: lector.id, email: lector.email };

    return {
      accessToken: await this.jwtService.signAsync(payload),
    };
  }

  async signIn({ email, password }: SignInRequestDto): Promise<SignInResponseDto> {
    const lector = await this.lectorsService.findLectorByEmail(email);

    if (!lector) throw new UnauthorizedException(`Lector does not exist.`);

    const isEqual = await this.hashingService.compare(password, lector.password);

    if (!isEqual) {
      throw new UnauthorizedException('Passwords do not match.');
    }

    const payload = { sub: lector.id, email: lector.email };
    return {
      accessToken: await this.jwtService.signAsync(payload),
    };
  }

  async resetPasswordRequest(email: string): Promise<void> {
    const lector = await this.lectorsService.findLectorByEmail(email);
    if (!lector) {
      throw new BadRequestException(`Lector with email: ${email} is not found.`);
    }

    const resetToken = await this.resetTokenService.generateResetToken(lector.id);

    await this.mailerService.sendResetPasswordEmail(email, resetToken, lector);
  }

  async resetPassword({
    lectorId,
    newPassword,
    token,
  }: ResetPasswordWithTokenRequestDto): Promise<void> {
    const resetToken = await this.resetTokenService.getResetToken(lectorId);

    const isValid = await this.hashingService.compare(token, resetToken.token);

    if (!isValid)
      throw new BadRequestException(`There is no reset password request for this lector.`);

    const lector = await this.lectorsService.findLectorWithCoursesById(lectorId);

    if (!lector) throw new BadRequestException(`Lector with { id: ${lectorId} } is not found.`);

    await this.lectorsService.updatePassword(lector.id, newPassword);

    await this.resetTokenService.removeResetTokenById(resetToken.id);
  }
}
