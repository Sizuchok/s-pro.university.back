import { MailerService } from '@nestjs-modules/mailer';
import { Injectable } from '@nestjs/common';
import { GenerateResetTokenResponseDto } from '../reset-token/dto/generate-reset-token-response.dto';
import { LectorEntityDto } from '../lectors/dto/lector-entity.dto';

@Injectable()
export class EmailerService {
  constructor(private mailerService: MailerService) {}

  async sendResetPasswordEmail(
    email: string,
    resetToken: GenerateResetTokenResponseDto,
    lector: LectorEntityDto,
  ): Promise<void> {
    const resetUrl = `${process.env.FRONT_END_URL}/reset-password?resetToken=${resetToken.token}&id=${resetToken.lectorId}`;

    await this.mailerService.sendMail({
      to: email,
      subject: 'S-Pro University Password Reset Request',
      template: 'reset-password',
      context: {
        name: lector.name,
        resetUrl,
      },
    });
  }
}
