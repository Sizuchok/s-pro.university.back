import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  HttpStatus,
  HttpCode,
  ParseIntPipe,
  Query,
} from '@nestjs/common';
import { GroupsService } from './groups.service';
import { CreateGroupDto } from './dto/requests/create-group.dto';
import { UpdateGroupDto } from './dto/requests/update-group.dto';
import {
  ApiBadRequestResponse,
  ApiBearerAuth,
  ApiInternalServerErrorResponse,
  ApiNoContentResponse,
  ApiNotFoundResponse,
  ApiOkResponse,
  ApiOperation,
  ApiResponse,
  ApiTags,
} from '@nestjs/swagger';
import { GetGroupWithStudentsResponseDto } from './dto/responses/get-group-with-students-response.dto';
import { CreateGroupResponseDto } from './dto/responses/create-group-response.dto';
import { UpdateGroupResponseDto } from './dto/responses/update-group-response.dto';
import { InternalServerErrorResponse } from '../application/dto/internal-server-error-response.dto';
import { GetGroupResponseDto } from './dto/responses/get-group-response.dto';
import { GroupsPaginationQueryDto } from './dto/groups-pagination-query.dto';
import { GetAllGroupsWithCount } from './dto/responses/get-all-groups-with-count.dto';

@ApiBearerAuth()
@ApiInternalServerErrorResponse({
  description: 'Internal server error',
  type: InternalServerErrorResponse,
})
@ApiTags('groups')
@Controller('groups')
export class GroupsController {
  constructor(private readonly groupsService: GroupsService) {}

  @ApiOperation({ summary: 'Create a group' })
  @ApiBadRequestResponse({
    description: 'Invalid group name.',
  })
  @ApiResponse({
    status: HttpStatus.CREATED,
    type: CreateGroupResponseDto,
    description: 'A freshly created group object.',
  })
  @Post()
  createGroup(@Body() createGroupDto: CreateGroupDto): Promise<CreateGroupResponseDto> {
    return this.groupsService.createGroup(createGroupDto);
  }

  @ApiOperation({ summary: 'Find all groups' })
  @ApiOkResponse({
    type: GetAllGroupsWithCount,
    isArray: true,
    description: 'An array of group objects.',
  })
  @Get()
  findAllGroups(
    @Query() groupsPaginationQueryDto: GroupsPaginationQueryDto,
  ): Promise<GetAllGroupsWithCount> {
    return this.groupsService.findAllGroups(groupsPaginationQueryDto);
  }

  @ApiOperation({ summary: 'Find all groups with their students' })
  @ApiOkResponse({
    type: GetGroupWithStudentsResponseDto,
    isArray: true,
    description: 'An array of group objects, each with its students array.',
  })
  @Get('/students')
  findAllGroupsWithStudents(): Promise<GetGroupWithStudentsResponseDto[]> {
    return this.groupsService.findAllGroupsWithStudents();
  }

  @ApiOperation({ summary: 'Find a group by id' })
  @ApiBadRequestResponse({
    description: 'Invalid id param',
  })
  @ApiNotFoundResponse({ description: 'Group with { id } not found.' })
  @ApiOkResponse({
    type: GetGroupResponseDto,
    description: 'A group object with students array.',
  })
  @Get(':id')
  findGroupById(@Param('id', ParseIntPipe) id: number): Promise<GetGroupResponseDto> {
    return this.groupsService.findGroupById(id);
  }

  @ApiOperation({ summary: 'Find a group by id with its students' })
  @ApiBadRequestResponse({
    description: 'Invalid id param.',
  })
  @ApiNotFoundResponse({ description: 'Group with { id } not found.' })
  @ApiOkResponse({
    type: GetGroupWithStudentsResponseDto,
    description: 'A group object with students array.',
  })
  @Get(':id/students')
  findGroupWithStudentsById(
    @Param('id', ParseIntPipe) id: number,
  ): Promise<GetGroupWithStudentsResponseDto> {
    return this.groupsService.findGroupWithStudentsById(id);
  }

  @ApiOperation({ summary: 'Update a group by id' })
  @ApiBadRequestResponse({
    description: 'Invalid id param or group name. | Group with { id } not found.',
  })
  @ApiOkResponse({
    type: UpdateGroupResponseDto,
    description: 'Updated group object.',
  })
  @Patch(':id')
  updateGroupById(
    @Param('id', ParseIntPipe) id: number,
    @Body() updateGroupDto: UpdateGroupDto,
  ): Promise<UpdateGroupResponseDto> {
    return this.groupsService.updateGroupById(id, updateGroupDto);
  }

  @ApiOperation({ summary: 'Remove a group by id' })
  @ApiBadRequestResponse({
    description: 'Invalid id param. | Group with { id } not found.',
  })
  @ApiNoContentResponse({
    description: 'Group successfully removed.',
  })
  @HttpCode(HttpStatus.NO_CONTENT)
  @Delete(':id')
  removeGroupById(@Param('id', ParseIntPipe) id: number): Promise<void> {
    return this.groupsService.removeGroupById(id);
  }
}
