import {
  BadRequestException,
  Injectable,
  InternalServerErrorException,
  NotFoundException,
} from '@nestjs/common';
import { CreateGroupDto } from './dto/requests/create-group.dto';
import { UpdateGroupDto } from './dto/requests/update-group.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { Group } from './entities/group.entity';
import { FindManyOptions, ILike, Repository } from 'typeorm';
import { UpdateGroupResponseDto } from './dto/responses/update-group-response.dto';
import { GetGroupWithStudentsResponseDto } from './dto/responses/get-group-with-students-response.dto';
import { GetGroupResponseDto } from './dto/responses/get-group-response.dto';
import { GroupsPaginationQueryDto } from './dto/groups-pagination-query.dto';
import { CreateGroupResponseDto } from './dto/responses/create-group-response.dto';
import { GetAllGroupsWithCount } from './dto/responses/get-all-groups-with-count.dto';

@Injectable()
export class GroupsService {
  constructor(@InjectRepository(Group) private groupsRepository: Repository<Group>) {}

  createGroup(createGroupDto: CreateGroupDto): Promise<CreateGroupResponseDto> {
    return this.groupsRepository.save(createGroupDto);
  }

  async findAllGroups({
    sortBy,
    order,
    limit,
    offset,
    query,
  }: GroupsPaginationQueryDto): Promise<GetAllGroupsWithCount> {
    const findOptions: FindManyOptions<Group> = {
      skip: offset,
      take: limit,
    };

    if (query) findOptions.where = [{ name: ILike(`%${query}%`) }];

    if (sortBy) findOptions.order = { [sortBy]: order };

    return {
      groups: await this.groupsRepository.find(findOptions),
      count: await this.groupsRepository.count(findOptions),
    };
  }

  findAllGroupsWithStudents(): Promise<GetGroupWithStudentsResponseDto[]> {
    return this.groupsRepository.find({
      relations: {
        students: true,
      },
    });
  }

  async findGroupById(id: number): Promise<GetGroupResponseDto> {
    const group = await this.groupsRepository.findOneBy({ id });

    if (!group) throw new NotFoundException(`Group with { id: ${id} } not found.`);
    return group;
  }

  async findGroupWithStudentsById(id: number): Promise<GetGroupWithStudentsResponseDto> {
    const group = await this.groupsRepository.findOne({
      where: {
        id,
      },
      relations: {
        students: true,
      },
    });

    if (!group) throw new NotFoundException(`Group with { id: ${id} } not found.`);
    return group;
  }

  async updateGroupById(
    id: number,
    updateGroupDto: UpdateGroupDto,
  ): Promise<UpdateGroupResponseDto> {
    const group = await this.groupsRepository.findOneBy({ id });

    if (!group) throw new BadRequestException(`Group with { id: ${id} } not found for update.`);

    const result = await this.groupsRepository.update(id, updateGroupDto);

    if (!result.affected) throw new InternalServerErrorException('Internal server error.');

    const updatedGroup = await this.groupsRepository.findOneBy({ id });

    if (!updatedGroup)
      throw new InternalServerErrorException('Error retrieving the updated group after update.');

    return updatedGroup;
  }

  async removeGroupById(id: number): Promise<void> {
    const group = await this.groupsRepository.findOneBy({ id });

    if (!group) throw new BadRequestException(`Group with { id: ${id} } not found for deletion.`);

    const result = await this.groupsRepository.delete(id);

    if (!result.affected) throw new InternalServerErrorException('Internal server error.');
  }
}
