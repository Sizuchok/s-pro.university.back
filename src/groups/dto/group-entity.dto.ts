import { ApiProperty } from '@nestjs/swagger';
import { MaxLength, MinLength, IsString } from 'class-validator';
import { CoreEntityDto } from '../../application/dto/core-entity.dto';

export class GroupEntityDto extends CoreEntityDto {
  @ApiProperty({ example: 'SP-12' })
  @MaxLength(10)
  @MinLength(2)
  @IsString()
  readonly name: string;
}
