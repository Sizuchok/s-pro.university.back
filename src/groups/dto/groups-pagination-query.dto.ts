import { ApiPropertyOptional } from '@nestjs/swagger';
import { BasePaginationQuery } from '../../application/dto/base-pagination-query.dto';
import { IsEnum, IsOptional } from 'class-validator';
import { BaseSortBy } from '../../application/dto/enums/base-query.enums';

export const GroupsSortBy = {
  ...BaseSortBy,
  name: 'name',
} as const;

export class GroupsPaginationQueryDto extends BasePaginationQuery {
  @ApiPropertyOptional({
    description: 'A field by which groups list should be sorted',
    example: 'createdAt',
  })
  @IsOptional()
  @IsEnum(GroupsSortBy, { message: 'Invalid sortBy parameter' })
  readonly sortBy: string;
}
