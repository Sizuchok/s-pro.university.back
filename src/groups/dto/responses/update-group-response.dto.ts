import { GroupEntityDto } from '../group-entity.dto';

export class UpdateGroupResponseDto extends GroupEntityDto {}
