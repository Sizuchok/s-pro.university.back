import { GroupEntityDto } from '../group-entity.dto';

export class GetGroupResponseDto extends GroupEntityDto {}
