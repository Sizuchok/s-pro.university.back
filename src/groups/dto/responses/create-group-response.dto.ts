import { GroupEntityDto } from '../group-entity.dto';

export class CreateGroupResponseDto extends GroupEntityDto {}
