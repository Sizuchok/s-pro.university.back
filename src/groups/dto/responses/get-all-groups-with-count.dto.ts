import { ApiProperty } from '@nestjs/swagger';
import { GetGroupResponseDto } from './get-group-response.dto';

export class GetAllGroupsWithCount {
  @ApiProperty()
  readonly groups: GetGroupResponseDto[];

  @ApiProperty({
    example: 1,
  })
  readonly count: number;
}
