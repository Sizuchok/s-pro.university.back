import { ApiProperty } from '@nestjs/swagger';
import { GroupEntityDto } from '../group-entity.dto';
import { StudentEntityDto } from '../../../students/dto/student-entity.dto';

export class GetGroupWithStudentsResponseDto extends GroupEntityDto {
  @ApiProperty({
    description: 'An array of students which belong to the group.',
    nullable: true,
  })
  readonly students: StudentEntityDto[];
}
