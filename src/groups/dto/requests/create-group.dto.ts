import { OmitType } from '@nestjs/swagger';
import { GroupEntityDto } from '../group-entity.dto';

export class CreateGroupDto extends OmitType(GroupEntityDto, [
  'id',
  'createdAt',
  'updatedAt',
] as const) {}
