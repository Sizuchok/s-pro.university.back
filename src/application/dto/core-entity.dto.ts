import { ApiProperty } from '@nestjs/swagger';
import { IsInt, IsPositive, IsNumber, IsDate } from 'class-validator';

export class CoreEntityDto {
  @ApiProperty({ example: 1 })
  @IsInt()
  @IsPositive()
  @IsNumber()
  readonly id: number;

  @IsDate()
  readonly createdAt: Date;

  @IsDate()
  readonly updatedAt: Date;
}
