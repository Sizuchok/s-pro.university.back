import { IsOptional, IsEnum, IsPositive, IsInt, IsString } from 'class-validator';
import { BaseOrder, BaseSortBy } from './enums/base-query.enums';
import { ApiPropertyOptional } from '@nestjs/swagger';

export class BasePaginationQuery {
  @ApiPropertyOptional({
    description: 'A search query',
    example: 'anything',
  })
  @IsString()
  @IsOptional()
  query: string;

  @ApiPropertyOptional({
    description: 'A field by which response data should be sorted',
    example: 'createdAt',
  })
  @IsOptional()
  @IsEnum(BaseSortBy, { message: 'Invalid sortBy parameter' })
  readonly sortBy: string;

  @ApiPropertyOptional({
    description: 'Specifies the order of sorting',
    example: 'desc',
  })
  @IsOptional()
  @IsEnum(BaseOrder, { message: 'Invalid order parameter' })
  readonly order: string;

  @ApiPropertyOptional({
    description: 'Specifies the amount of entity rows to be returned',
    example: 10,
  })
  @IsOptional()
  @IsPositive()
  @IsInt()
  readonly limit: number;

  @ApiPropertyOptional({
    description: 'Specifies the amount of entity rows to be skipped',
    example: 1,
  })
  @IsOptional()
  @IsPositive()
  @IsInt()
  readonly offset: number;
}
