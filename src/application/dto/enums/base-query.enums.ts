export const BaseSortBy = {
  createdAt: 'createdAt',
  updatedAt: 'updatedAt',
} as const;

export const BaseOrder = {
  asc: 'asc',
  desc: 'desc',
} as const;
