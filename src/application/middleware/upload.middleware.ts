import { diskStorage } from 'multer';
import { join } from 'path';
import * as fs from 'fs';

const imageDestinition = join(__dirname, '../../', 'temp');
const publicFolder = join(__dirname, '../../', 'public');

if (!fs.existsSync(imageDestinition)) {
  fs.mkdirSync(imageDestinition);
}

if (!fs.existsSync(publicFolder)) {
  fs.mkdirSync(publicFolder);
}

export const uploadMiddleware = {
  storage: diskStorage({
    destination: imageDestinition,
    filename: (request, file, savesAs) => {
      const fileName = `${Date.now()}-${file.originalname}`;
      savesAs(null, fileName);
    },
  }),
};
