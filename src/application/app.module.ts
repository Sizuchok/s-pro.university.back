import { MailerModule } from '@nestjs-modules/mailer';
import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AuthModule } from '../auth/auth.module';
import { typeOrmAsyncConfig } from '../configs/database/typeorm-config';
import { CoursesModule } from '../courses/courses.module';
import { GroupsModule } from '../groups/groups.module';
import { HashingModule } from '../hashing/hashing.module';
import { LectorsModule } from '../lectors/lectors.module';
import { MarksModule } from '../marks/marks.module';
import { StudentsModule } from '../students/students.module';
import { mailerConfig } from '../configs/mailer/mailer-config';
import { ServeStaticModule } from '@nestjs/serve-static';
import { join } from 'path';

@Module({
  imports: [
    MailerModule.forRootAsync({ useFactory: () => mailerConfig }),
    TypeOrmModule.forRootAsync(typeOrmAsyncConfig),
    ConfigModule.forRoot({
      isGlobal: true,
    }),
    ServeStaticModule.forRoot({
      rootPath: join(__dirname, '../', 'public'),
      renderPath: '/public',
    }),
    GroupsModule,
    StudentsModule,
    LectorsModule,
    CoursesModule,
    MarksModule,
    AuthModule,
    HashingModule,
  ],
})
export class AppModule {}
